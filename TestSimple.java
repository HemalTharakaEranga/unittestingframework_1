package kunitDemo1;
import static kunit1.KUnit.*;
import org.apache.logging.log4j.*;





/******************************************************************************
 * This code demonstrates the use of the KUnit testing tool. It produces a
 * report that contains messages generated from the check methods.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class TestSimple {
	
	private static final Logger logger = LogManager.getLogger(TestSimple.class);

  void checkConstructorAndAccess(){
    Simple s = new Simple(3.0, 4.0);
    checkEquals(s.getA(),4.0);
    checkEquals(s.getB(), 4.0);
    checkNotEquals(s.getB(), 4.0);    
    checkNotEquals(s.getB(), 5.0);    
  }

  void checkSquareA(){
    Simple s = new Simple(3.0, 4.0);
    s.squareA();
    checkEquals(s.getA(), 9.0);
  }


  public static void main(String[] args) {
    //TestSimple ts = new TestSimple();
    //ts.checkConstructorAndAccess();
    //ts.checkSquareA();
    //report();
	  
      TestSimple ts = new TestSimple();
      ts.checkConstructorAndAccess();
      ts.checkSquareA();
      report();

      System.setProperty("log4j.configurationFile", "log4j2.xml");
      //logger.info("Starting Check.......!");
 

      //logger.info("Check completed.");
  }
}
