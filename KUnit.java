package kunit1;

import java.util.*;

import org.apache.logging.log4j.*;


/******************************************************************************
 * This code is a prototype of a unit testing system. It is very primitive and 
 * contains only a reporting system and a means of checking assertions.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class KUnit {
  private static List<String> checks;
  private static double checksMade = 1;
  private static double passedChecks = 0;
  private static double failedChecks = 0;
  private static final Logger logger = LogManager.getLogger(KUnit.class);
  /******************************************************************************
   * A report is formed from a list of strings. This method adds a message to the
   * report. 
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/
  //private static void addToReport(String txt) {
	    //if (checks == null) {
	        //checks = new LinkedList<String>();
	    //}
	    //checks.add(String.format("%04d: %s", checksMade++, txt));
	//}
  private static void addToReport(String txt) {
    if (checks == null) {
      checks = new LinkedList<String>();
    }
    //checks.add(String.format("%.2f: %s", checksMade++, txt));
    logger.trace(String.format("%02d: %s", (int)checksMade++, txt));
  }

  /******************************************************************************
   * This method is similar to an assertion in JUnit. It checks that two integers
   * are equal and adds an appropriate message to the report.
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/
  
  public static void checkEquals(double value1, double value2) {
    if (value1 == value2) {
      addToReport(String.format("  %.2f == %.2f", value1, value2));
      passedChecks++;
    } else {
      addToReport(String.format("* %.2f == %.2f", value1, value2));
      failedChecks++;
    }
  }

  /******************************************************************************
   * This method is similar to an assertion in JUnit. It checks that two integers
   * are not equal and adds an appropriate message to the report.
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/

  public static void checkNotEquals(double value1, double value2) {
    if (value1 != value2) {
      addToReport(String.format("  %.2f != %.2f", value1, value2));
      passedChecks++;
    } else {
      addToReport(String.format("* %.2f != %.2f", value1, value2));
      failedChecks++;
    }
  }

  /******************************************************************************
   * Outputs the messages that form the report.
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/

  public static void report() {
	  
	    
	    logger.trace(" ");
	    logger.trace(String.format("%.2f checks passed", passedChecks));
	    logger.trace(String.format("%.2f checks failed", failedChecks));


	    for (String check : checks) {
	        logger.trace(check);
	    }
    //System.out.printf("%d checks passed\n", passedChecks);
    //System.out.printf("%d checks failed\n", failedChecks);
    //System.out.printf("%.2f: checks passed\n", passedChecks);
    //System.out.printf("%.2f  checks failed\n", failedChecks);
    //System.out.println();
    
    //for (String check : checks) {
      //System.out.println(check);
    //}

  }
}
